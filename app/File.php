<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\{News, Gallery};

class File extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'id', 'path', 'thumbnail'
	];

	public function checkUsage(){
		$id = $this->id;

		$galleries = Gallery::whereHas('files', function($q) use ($id)
		{
			if ($id) {
				$q->where('id', '=', $id);
			}
		})->get();

		$news = News::whereHas('files', function($q) use ($id)
		{
			if ($id) {
				$q->where('id', '=', $id);
			}
		})->get();

		$ids = array();

		foreach ($galleries as $item) {
			$ids[] = $item->id;
		}

		foreach ($news as $item) {
			$ids[] = $item->id;
		}

		return !empty($ids);
	}

	public function delete()
	{
		unlink(public_path() . $this->thumbnail);
		unlink(public_path() . $this->path);

		return parent::delete();
	}


	protected $table = 'files';
}
