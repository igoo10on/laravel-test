<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'id', 'uuid', 'description', 'created_at' , 'updated_at'
	];

	public function files()
	{
		return $this->belongsToMany('App\File', 'gallery_files',
			'gallery_id', 'file_id');
	}

	public function tags()
	{
		return $this->belongsToMany('App\Tag', 'gallery_tags',
			'gallery_id', 'tag_id');
	}

	protected $table = 'galleries';
}
