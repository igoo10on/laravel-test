<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\{
	News, Gallery
};


class ApiController extends Controller
{
    public function news(Request $request){

			$date = $request->get('date') ?? '';
			$head = $request->get('header') ?? '';

			$news = DB::table('news')
				->leftJoin('news_files','news_files.news_id','=','news.id')
				->leftJoin('files','files.id','=','news_files.file_id')
				->select('news.*' , 'files.path', 'files.thumbnail');

			if (!empty($head)) {
				$news->where('news.header','like','%' . $head . '%');
			}

			if (!empty($date)) {
				$dateStart = date('Y-m-d ', strtotime($date)) . '00:00:00';
				$dateEnd = date('Y-m-d ', strtotime($date)) . '23:59:59';
				$news->whereBetween('news.created_at',[$dateStart,$dateEnd]);
			}

			$news = $news->get();

			$response = array();

			foreach ($news as $item){
				$response[$item->id]['id'] = $item->id;
				$response[$item->id]['slug'] = $item->slug;
				$response[$item->id]['header'] = $item->header;
				$response[$item->id]['content'] = $item->content;
				$response[$item->id]['created_at'] = $item->created_at;
				$response[$item->id]['main_image'] = $request->root() . $item->path;
				$response[$item->id]['preview'] = $request->root() . $item->thumbnail;
			}

			return response()->json($response);
		}

		public function gallery(Request $request){

			$tag = $request->get('tag');

			if ($tag) {
				$galleries = Gallery::whereHas('tags', function ($q) use ($tag) {
					$q->where('id', '=', $tag);
				})->get();
			} else {
				$galleries = Gallery::get();
			}

			$response = array();

			foreach ($galleries as $item){
				$response[$item->id]['uuid'] = $item->uuid;
				$response[$item->id]['tags'] = $item->tags->pluck('name','id');
				$response[$item->id]['description'] = $item->description;
				$response[$item->id]['img'] = $request->root() . $item->files[0]->path;
			}

			return response()->json($response);
		}

}
