<?php

namespace App\Http\Controllers;

use Image;
use Illuminate\Http\Request;
use App\{
	Gallery, File, Tag
};
use Webpatser\Uuid\Uuid as Uuid;

class GalleryController extends Controller {

	public function index() {
		$galleries = Gallery::get();
		return view('gallery/main', ['galleries' => $galleries]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$tags = Tag::get();
		return view('gallery/create', ['tags' => $tags]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $readdir ()quest
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function save(Request $request) {

		$request->validate([
			'description' => 'required',
			'img' => 'mimes:jpg,jpeg,png'
		]);

		$uploadsDir = "/uploads/images/";
		$root = public_path() . $uploadsDir;

		$item = new Gallery;
		$item->description = $request->description;
		$item->uuid = (string) Uuid::generate();
		$item->save();

		$item->tags()->attach($request->tags);

		if ($request->hasFile('img')) {
			$imgFile = $request->file('img');
			if ($imgFile->isValid()) {
				$fileName = uniqid() . '.' . $imgFile->getClientOriginalExtension();
				$imgFile->move($root, $fileName);
				$file = new File;
				$file->path = $uploadsDir . $fileName;
				$file->thumbnail = $uploadsDir . 'thumbnails/' . $fileName;
				$file->save();

				$img = Image::make($root . $fileName);

				$img->resize(100, 100, function ($constraint) {
					$constraint->aspectRatio();
				})->save($root . 'thumbnails/' . $fileName);
				$item->files()->attach($file->id);
			}
		}

		return redirect('/gallery');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$item = Gallery::find($id);
		$tags = Tag::get();

		return view('gallery/edit', ['item' => $item, 'tags' => $tags]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {

		$request->validate([
			'description' => 'required',
		]);

		$uploadsDir = "/uploads/images/";
		$root = public_path() . $uploadsDir;

		$item = Gallery::find($id);
		$item->description = $request->description;
		$item->tags()->detach();
		$item->tags()->attach($request->tags);

		if (isset($request->img_update)) {
			$request->validate([
				'img' => 'mimes:jpg,jpeg,png'
			]);
			if ($request->hasFile('img')) {
				$imgFile = $request->file('img');
				if ($imgFile->isValid()) {
					$fileName = uniqid() . '.' . $imgFile->getClientOriginalExtension();
					$imgFile->move($root, $fileName);
					$file = new File;
					$file->path = $uploadsDir . $fileName;
					$file->thumbnail = $uploadsDir . 'thumbnails/' . $fileName;
					$file->save();

					$img = Image::make($root . $fileName);

					$img->resize(100, 100, function ($constraint) {
						$constraint->aspectRatio();
					})->save($root . 'thumbnails/' . $fileName);

					$oldFile = $item->files()->get();
					$oldFile = File::find($oldFile[0]->id);
					$item->files()->detach();

					if (!$oldFile->checkUsage()) {
						$oldFile->delete();
					}

					$item->files()->attach($file->id);
				}
			}
		}

		$item->save();

		return redirect('/gallery');
	}

	/**
	 * Show the form for delete a resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function delete($id) {
		$item = Gallery::find($id);
		return view('gallery/delete', ['item' => $item]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request, $id) {
		Gallery::findOrFail($id)->delete();
		return redirect('/gallery');
	}
}
