<?php

namespace App\Http\Controllers;

use Image;
use Illuminate\Http\Request;
use App\{
	News, File, Gallery
};

class NewsController extends Controller
{
    public function index(){
			$news = News::get();
			return view('news/main', ['news' => $news]);
		}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create(){
		$galleries = Gallery::get();
		return view('news/create', ['galleries' => $galleries]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $readdir()quest
	 * @return \Illuminate\Http\Response
	 */
	public function save(Request $request){

		$request->validate([
			'html' => 'required',
			'slug' => 'required',
			'header' => 'required',
			'img' => 'mimes:jpg,jpeg,png'
		]);

		$uploadsDir = "/uploads/images/";
		$root = public_path() . $uploadsDir;

		$item = new News;
		$item->content = $request->html;
		$item->slug = $request->slug;
		$item->header = $request->header;
		$item->save();

		switch ($request->img_mode){
			case 'upload':
				if ( $request->hasFile('img') ) {
					$imgFile = $request->file('img');
					if ( $imgFile->isValid() ) {
						$fileName = uniqid() . '.' . $imgFile->getClientOriginalExtension();
						$imgFile->move($root, $fileName);
						$file = new File;
						$file->path = $uploadsDir . $fileName;
						$file->thumbnail = $uploadsDir . 'thumbnails/' . $fileName;
						$file->save();

						$img = Image::make($root . $fileName);

						$img->resize(100, 100, function ($constraint) {
							$constraint->aspectRatio();
						})->save($root . 'thumbnails/' . $fileName);

						$item->files()->attach($file->id);
					}
				}
				break;

			case 'select':
				$item->files()->attach($request->gallery);
				break;
		}

		return redirect('/news');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id){
		$item = News::find($id);
		$galleries = Gallery::get();

		return view('news/edit',['item' => $item, 'galleries' => $galleries]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id){

		$request->validate([
			'html' => 'required',
			'slug' => 'required',
			'header' => 'required',
		]);

		$uploadsDir = "/uploads/images/";
		$root = public_path() . $uploadsDir;

		$item = News::find($id);
		$item->content = $request->html;
		$item->slug = $request->slug;
		$item->header = $request->header;

		if (isset($request->img_update)) {
			switch ($request->img_mode){
				case 'upload':
					$request->validate([
						'img' => 'mimes:jpg,jpeg,png'
					]);
					if ( $request->hasFile('img') ) {
						$imgFile = $request->file('img');
						if ( $imgFile->isValid() ) {
							$fileName = uniqid() . '.' . $imgFile->getClientOriginalExtension();
							$imgFile->move($root, $fileName);
							$file = new File;
							$file->path = $uploadsDir . $fileName;
							$file->thumbnail = $uploadsDir . 'thumbnails/' . $fileName;
							$file->save();

							$img = Image::make($root . $fileName);

							$img->resize(100, 100, function ($constraint) {
								$constraint->aspectRatio();
							})->save($root . 'thumbnails/' . $fileName);

							$item->files()->detach();
							$item->files()->attach($file->id);
						}
					}
					break;

				case 'select':
					$item->files()->detach();
					$item->files()->attach($request->gallery);
					break;
			}
		}

		$item->save();

		return redirect('/news');
	}

	/**
	 * Show the form for delete a resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function delete($id){
		$item = News::find($id);
		return view('news/delete',['item'=>$item]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request, $id){
		News::findOrFail($id)->delete();
		return redirect('/news');
	}
}
