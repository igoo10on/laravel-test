<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tag;

class TagsController extends Controller
{
    public function index(){
			$tags = Tag::get();
			return view('tags/main', ['tags' => $tags]);
		}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create(){
		return view('tags/create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $readdir()quest
	 * @return \Illuminate\Http\Response
	 */
	public function save(Request $request){

		$request->validate([
			'name' => 'required',
		]);

		$item = new Tag();
		$item->name = $request->name;
		$item->save();

		return redirect('/tags');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id){
		$item = Tag::find($id);
		return view('tags/edit',['item' => $item]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id){

		$request->validate([
			'name' => 'required',
		]);

		$item = Tag::find($id);
		$item->name = $request->name;
		$item->save();

		return redirect('/tags');
	}

	/**
	 * Show the form for delete a resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function delete($id){
		$item = Tag::find($id);
		return view('tags/delete',['item' => $item]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request, $id){
		Tag::findOrFail($id)->delete();
		return redirect('/tags');
	}
}
