<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class md5AuthController extends Controller
{
    public function index(){
				return view('md5Auth');
		}

		public function auth(Request $request){

				session_start();

				if (md5($request->password) == '21232f297a57a5a743894a0e4a801fc3') {
					$_SESSION['admin'] = true;
				}

				return redirect('/news');
		}

}
