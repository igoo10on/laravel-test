<?php

namespace App\Http\Middleware;

use Closure;

class md5Auth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    		session_start();
    		if (!isset($_SESSION['admin'])) {
					return redirect('/auth');
				}

        return $next($request);
    }
}
