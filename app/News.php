<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'slug', 'header', 'content', 'created_at' , 'updated_at'
	];

	public function files()
	{
		return $this->belongsToMany('App\File', 'news_files',
			'news_id', 'file_id');
	}

	protected $table = 'news';
}
