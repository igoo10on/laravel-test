<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Files extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->increments('id');
						$table->string('path');
						$table->string('thumbnail')->nullable();
						$table->timestamps();
        });

				Schema::create('gallery_files', function (Blueprint $table) {
					$table->integer('gallery_id')->unsigned()->index();
					$table->foreign('gallery_id')->references('id')->on('galleries')->onDelete('cascade');
					$table->integer('file_id')->unsigned()->index();
					$table->foreign('file_id')->references('id')->on('files')->onDelete('cascade');
					$table->timestamps();
				});

				Schema::create('news_files', function (Blueprint $table) {
					$table->integer('news_id')->unsigned()->index();
					$table->foreign('news_id')->references('id')->on('news')->onDelete('cascade');
					$table->integer('file_id')->unsigned()->index();
					$table->foreign('file_id')->references('id')->on('files')->onDelete('cascade');
					$table->timestamps();
				});

				$path = public_path() . '/uploads/images/thumbnails/';
				File::makeDirectory($path, $mode = 0777, true, true);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
				Schema::dropIfExists('news_files');
				Schema::dropIfExists('gallery_files');
				Schema::dropIfExists('files');
		}
}
