$(document).ready(function() {
  $(".chosen-select").chosen();

  var $imgUpdate = $('input[name="img_update"]');

  if ($imgUpdate.length > 0) {
    $('.gallery-update-image').hide();
    $imgUpdate.on('change',function (e) {
      $('.gallery-update-image').toggle();
    });
  }

});
