$(document).ready(function() {
  var $imgMode = $('input[name="img_mode"]');

  if ($imgMode.length > 0) {
    var imgMode = $imgMode.val();

    if (imgMode === 'upload') {
      $('.news-gallery-image').hide();
    } else {
      $('.news-upload-image').hide();
    }

    $imgMode.on('change',function (e) {
      $('.news-upload-image').toggle();
      $('.news-gallery-image').toggle();
    });
  }

  var $imgUpdate = $('input[name="img_update"]');

  if ($imgUpdate.length > 0) {
    $('.news-update-image').hide();
    $imgUpdate.on('change',function (e) {
      $('.news-update-image').toggle();
    });
  }

});