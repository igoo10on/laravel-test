@extends('layouts.app')
@section('title')
    Добавить галлерею
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12 panel panel-default">
            <form enctype="multipart/form-data" action="{{ URL::to('/gallery/create') }}" method="POST" class="form-horizontal">
                {{ csrf_field() }}
                <div class="form-group">
                    <div class="col-md-offset-1 col-md-8 form-add">
                        <div class="form-group">
                            <h3>
                                Создание галлереи
                            </h3>
                            <label for="description">
                                Описание
                                <input type="text" name="description" id="description" class="form-control">
                            </label>
                        </div>
                        <div class="form-group">
                        <label for="img">
                            Изображение для галлереи
                            <input type="file" name="img" id="img">
                        </label>
                        </div>
                        <div class="form-group">
                            <label>
                                <select class="chosen-select" name="tags" multiple>
                                    @foreach ( $tags as $tag )
                                        <option value="{{$tag->id}}">{{$tag->name}}</option>
                                    @endforeach
                                </select>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-1 col-sm-6">
                        <button type="submit" class="btn btn-default">
                            <i class="fa fa-plus"></i> Создать галлерею
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
