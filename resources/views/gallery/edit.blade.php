@extends('layouts.app')
@section('title')
    Редактировать галлерею
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12 panel panel-default">
            <form enctype="multipart/form-data" action="{{ URL::to('/gallery/' . $item->id . '/edit') }}" method="POST" class="form-horizontal">
                {{ csrf_field() }}
                <div class="form-group">
                    <div class="col-md-offset-1 col-md-8 form-add">
                        <div class="form-group">
                            <h3>
                                Правка галлереи
                            </h3>
                            <label for="description">
                                Имя
                                <input type="text" name="description" id="description" value="{{ $item->description }}" class="form-control">
                            </label>
                            <br/>
                            <label for="img_update">
                                Загрузить новое изображение
                                <input type="checkbox" name="img_update" id="img_update" value="1" class="form-control">
                            </label>
                        </div>
                        <div class="form-group gallery-update-image">
                            <label for="img">
                                Новое изображение
                                <input type="file" name="img" id="img">
                            </label>
                        </div>
                        <label>
                            <select class="chosen-select" name="tags" multiple>
                            @foreach ( $tags as $tag )
                                    <option value="{{$tag->id}}"
                                    @if (in_array($tag->id,$item->tags->pluck('id')->toArray()))
                                    selected
                                    @endif>{{$tag->name}}</option>
                            @endforeach
                            </select>
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-1 col-sm-6">
                        <button type="submit" class="btn btn-default">
                            <i class="fa fa-plus"></i> Сохранить
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
