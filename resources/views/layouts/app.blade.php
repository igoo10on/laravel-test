<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>API Админка</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ elixir('css/style.css') }}" rel="stylesheet">
    <link href="{{ elixir('/js/chosen/chosen.min.css') }}" rel="stylesheet">

    <style>
        body {
            font-family: 'Lato';
        }
    </style>
</head>
<body id="app-layout">
<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">

            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/') }}">
                Админка
            </a>
        </div>

    </div>
</nav>

<div class="container">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <ul class="nav nav-tabs">
                <li class="{{ Request::is('news*') ? 'active' : '' }}">
                    <a href="/news">
                        Новости <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                    </a>
                </li>
                <li class="{{ Request::is('gallery*') ? 'active' : '' }}">
                    <a href="/gallery">
                        Галлереи <i class="fa fa-picture-o" aria-hidden="true"></i>
                    </a>
                </li>
                <li class="{{ Request::is('tags*') ? 'active' : '' }}">
                    <a href="/tags">
                        Теги <i class="fa fa-tags" aria-hidden="true"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="col-xs-12 col-md-12">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @yield('content')
        </div>
    </div>
</div>


<!-- JavaScripts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="{{ asset('/js/chosen/chosen.jquery.min.js') }}" type="text/javascript" charset="utf-8" ></script>
<script src="{{ asset('js/news.js') }}"></script>
<script src="{{ asset('js/gallery.js') }}"></script>
</body>
</html>
