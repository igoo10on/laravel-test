@extends('layouts.app')
@section('title')
    Авторизация
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <form enctype="multipart/form-data" action="{{ URL::to('/auth') }}" method="POST" class="form-horizontal">
                {{ csrf_field() }}
                <div class="form-group">
                    <div class="col-md-offset-1 col-md-8 form-add">
                        <label for="name">
                            Пароль
                            <input type="password" name="password" id="password" class="form-control">
                        </label>
                        <br/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-1 col-sm-6">
                        <button type="submit" class="btn btn-default">
                            <i class="fa fa-user"></i> Войти
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
