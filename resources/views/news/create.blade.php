@extends('layouts.app')
@section('title')
    Create new product
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12 panel panel-default">
            <form enctype="multipart/form-data" action="{{ URL::to('/news/create') }}" method="POST" class="form-horizontal">
                {{ csrf_field() }}
                <div class="form-group">
                    <div class="col-md-offset-1 col-md-8 form-add">
                        <div class="form-group">
                            <h3>
                                Создание новости
                            </h3>
                            <label for="header">
                                Заголовок
                                <input type="text" name="header" id="header" class="form-control">
                            </label>
                        </div>
                        <div class="form-group">
                            <label for="slug">
                                ЧПУ
                                <input type="text" name="slug" id="slug" class="form-control">
                            </label>
                        </div>
                        <div class="form-group">
                            <label for="content">
                                Содержимое
                                <textarea rows="5" name="html" id="content" class="form-control" placeholder="Html content"></textarea>
                            </label>
                        </div>
                        <div class="form-group">
                            <h4><b>Изобржение</b></h4>
                            <br/>
                            <label>
                                Загрузить используя форму
                                <input type="radio" name="img_mode" value="upload" checked>
                            </label>
                            <br/>
                            <label>
                                Выбрать из галлереи
                                <input type="radio" name="img_mode" value="select">
                            </label>
                        </div>
                        <div class="form-group news-upload-image">
                            <label for="img">
                                Загрузить изображение для новости
                                <input type="file" name="img" id="img">
                            </label>
                        </div>
                        <div>Изображения галлерей</div>
                        <div class="form-group news-gallery-image">
                            @foreach ($galleries as $item)
                                <label class="gallery-checkbox" style="background-image: url({{ URL::to($item->files[0]->thumbnail) }});">
                                    <input type="radio" name="gallery" value="{{$item->files[0]->id}}">
                                    <spna class="image-checked-overlay"></spna>
                                </label>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-1 col-sm-6">
                        <button type="submit" class="btn btn-default">
                            <i class="fa fa-plus"></i> Создать новость
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script src="{{ asset('/js/ckeditor/ckeditor.js') }}"
            type="text/javascript" charset="utf-8" ></script>
    <script>
      var editor = CKEDITOR.replace( 'content' );
    </script>
@endsection
