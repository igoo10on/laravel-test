@extends('layouts.app')
@section('title')
    Create new product
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <form enctype="multipart/form-data" action="{{ URL::to('/news/' . $item->id . '/edit') }}" method="POST" class="form-horizontal">
                {{ csrf_field() }}
                <div class="form-group">
                    <div class="col-md-offset-1 col-md-8 form-add">
                        <div class="form-group">
                            <h3>
                                Редактирование новости
                            </h3>
                            <label for="header">
                                Заголовок
                                <input type="text" name="header" id="header" value="{{ $item->header }}" class="form-control">
                            </label>
                            <br/>
                            <label for="slug">
                                ЧПУ
                                <input type="text" name="slug" id="slug" value="{{ $item->slug }}" class="form-control">
                            </label>
                            <br/>
                            <label for="content">
                                Содержимое
                                <textarea rows="5" name="html" id="content" class="form-control" placeholder="Html content">{{ $item->content }}</textarea>
                            </label>
                            <br/>
                            <label for="img_update">
                                Загрузить новое изображение
                                <input type="checkbox" name="img_update" id="img_update" value="1" class="form-control">
                            </label>
                        </div>
                        <div class="form-group news-update-image">
                            Изобржение
                            <br/>
                            <label>
                                Загрузить используя форму
                                <input type="radio" name="img_mode" value="upload" checked>
                            </label>
                            <br/>
                            <label>
                                Выбрать из галлереи
                                <input type="radio" name="img_mode" value="select">
                            </label>
                            <br/>

                            <label for="img" class="news-upload-image">
                                Загрузить изображение для новости
                                <input type="file" name="img" id="img">
                            </label>
                            <div class="form-group news-gallery-image">
                                @foreach ($galleries as $item)
                                    <label class="gallery-checkbox" style="background-image: url({{ URL::to($item->files[0]->thumbnail) }});">
                                        <input type="radio" name="gallery" value="{{$item->files[0]->id}}">
                                        <spna class="image-checked-overlay"></spna>
                                    </label>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-6">
                        <button type="submit" class="btn btn-default">
                            <i class="fa fa-plus"></i> Обновить новость
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script src="{{ asset('/js/ckeditor/ckeditor.js') }}"
            type="text/javascript" charset="utf-8" ></script>
    <script>
      var editor = CKEDITOR.replace( 'content' );
    </script>
@endsection
