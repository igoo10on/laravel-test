@extends('layouts.app')

@section('content')
        <div class="panel panel-default">
            <div class="panel-heading">
                Новости
            </div>

            <div class="panel-body">
                @if (count($news) > 0)

                <table class="table table-striped task-table">

                    <thead>
                    <th>Заголовок</th>
                    <th>ЧПУ</th>
                    <th>Миниатюра</th>
                    <th>Операции</th>
                    </thead>

                    <tbody>
                    @foreach ($news as $item)
                        <tr>
                            <td class="table-text">
                                <div>{{ $item->header }}</div>
                            </td>

                            <td class="table-text">
                                <div>{{ $item->slug }}</div>
                            </td>

                            <td class="table-text">
                                <div>
                                    <img src="{{ URL::to($item->files[0]->thumbnail) }}" width="50" height="50"/>
                                </div>
                            </td>

                            <td>
                                <div>
                                    <a href="{{ URL::to('/news/' . $item->id . '/delete') }}">
                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                        Удалить
                                    </a>
                                    <br/>
                                    <a href="{{ URL::to('/news/' . $item->id . '/edit') }}">
                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                        Редактировать
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                @endif
            </div>
        </div>
        <div class="col-md-2">
            <a href="/news/create" class="btn btn-block btn-primary"><i class="glyphicon glyphicon-plus"></i>Добавить новость</a>
        </div>
@endsection