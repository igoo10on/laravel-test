@extends('layouts.app')
@section('title')
    Подтверждение
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12 panel panel-default">
            <form enctype="multipart/form-data" action="{{ URL::to('/tags/' . $item->id . '/delete') }}" method="POST" class="form-horizontal">
                {{ csrf_field() }}
                <div class="form-group">
                    <div class="col-md-offset-1 col-md-8 form-add">
                        <h3>
                            {{$item->name}}
                        </h3>
                        Вы действительно хотите удалить тег?
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-1 col-sm-6">
                        <button type="submit" class="btn btn-default">
                            <i class="fa fa-trash"></i> Удалить
                        </button>
                        <a href="{{ URL::to('/news')}}" class="btn btn-default">
                            <i class="fa fa-undo"></i> Вернуться
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
