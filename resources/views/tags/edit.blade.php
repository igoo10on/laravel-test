@extends('layouts.app')
@section('title')
    Правка тега
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12 panel panel-default">
            <form enctype="multipart/form-data" action="{{ URL::to('/tags/' . $item->id . '/edit') }}" method="POST" class="form-horizontal">
                {{ csrf_field() }}
                <div class="form-group">
                    <div class="col-md-offset-1 col-md-8 form-add">
                        <h3>
                            Редактирование тега
                        </h3>
                        <label for="name">
                            Имя
                            <input type="text" name="name" id="name" value="{{ $item->name }}" class="form-control">
                        </label>
                        <br/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-1 col-sm-6">
                        <button type="submit" class="btn btn-default">
                            <i class="fa fa-plus"></i> Сохранить
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
