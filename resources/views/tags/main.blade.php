@extends('layouts.app')

@section('content')
        <div class="panel panel-default">
            <div class="panel-heading">
                Теги
            </div>

            <div class="panel-body">
                @if (count($tags) > 0)

                <table class="table table-striped task-table">

                    <thead>
                    <th>Название</th>
                    <th>Операции</th>
                    </thead>

                    <tbody>
                    @foreach ($tags as $item)
                        <tr>
                            <td class="table-text">
                                <div>{{ $item->name }}</div>
                            </td>

                            <td>
                                <div>
                                    <a href="{{ URL::to('/tags/' . $item->id . '/delete') }}">
                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                        Удалить
                                    </a>
                                    <br/>
                                    <a href="{{ URL::to('/tags/' . $item->id . '/edit') }}">
                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                        Редактировать
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                @endif
            </div>
        </div>
        <div class="col-md-2">
            <a href="/tags/create" class="btn btn-block btn-primary"><i class="glyphicon glyphicon-plus"></i>Создать тег</a>
        </div>
@endsection