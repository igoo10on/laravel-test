<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'NewsController@index')->middleware(['md5Auth']);

Route::get('/auth', 'md5AuthController@index');

Route::post('/auth', 'md5AuthController@auth');


/*
|--------------------------------------------------------------------------
| News Routes
|--------------------------------------------------------------------------
*/

Route::get('/news', 'NewsController@index')->middleware(['md5Auth']);


Route::get('/news/create', 'NewsController@create')->middleware(['md5Auth']);

Route::post('/news/create', 'NewsController@save')->middleware(['md5Auth']);


Route::get('/news/{id}/edit', 'NewsController@edit')->middleware(['md5Auth']);

Route::post('/news/{id}/edit', 'NewsController@update')->middleware(['md5Auth']);


Route::get('/news/{id}/delete', 'NewsController@delete')->middleware(['md5Auth']);

Route::post('/news/{id}/delete', 'NewsController@destroy')->middleware(['md5Auth']);

/*
|--------------------------------------------------------------------------
| Tags Routes
|--------------------------------------------------------------------------
*/

Route::get('/tags', 'TagsController@index')->middleware(['md5Auth']);


Route::get('/tags/create', 'TagsController@create')->middleware(['md5Auth']);

Route::post('/tags/create', 'TagsController@save')->middleware(['md5Auth']);


Route::get('/tags/{id}/edit', 'TagsController@edit')->middleware(['md5Auth']);

Route::post('/tags/{id}/edit', 'TagsController@update')->middleware(['md5Auth']);


Route::get('/tags/{id}/delete', 'TagsController@delete')->middleware(['md5Auth']);

Route::post('/tags/{id}/delete', 'TagsController@destroy')->middleware(['md5Auth']);

/*
|--------------------------------------------------------------------------
| Gallery Routes
|--------------------------------------------------------------------------
*/

Route::get('/gallery', 'GalleryController@index')->middleware(['md5Auth']);


Route::get('/gallery/create', 'GalleryController@create')->middleware(['md5Auth']);

Route::post('/gallery/create', 'GalleryController@save')->middleware(['md5Auth']);


Route::get('/gallery/{id}/edit', 'GalleryController@edit')->middleware(['md5Auth']);

Route::post('/gallery/{id}/edit', 'GalleryController@update')->middleware(['md5Auth']);


Route::get('/gallery/{id}/delete', 'GalleryController@delete')->middleware(['md5Auth']);

Route::post('/gallery/{id}/delete', 'GalleryController@destroy')->middleware(['md5Auth']);

/*
|--------------------------------------------------------------------------
| Api Routes
|--------------------------------------------------------------------------
*/

Route::get('api/gallery', 'ApiController@gallery');

Route::get('api/news', 'ApiController@news');
